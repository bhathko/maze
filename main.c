#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int **maze;
int width;
int height;

struct Position {
    int x;
    int y;
};

struct Position start;
struct Position end;



void getMazeWidthAndHeight(const char* line, int length) {
    int space;
    for (int i = 0; i < length; i ++) {
        if ((int)line[i] == 32) {
            space = i;
        }
    }
    char *widthStr = (char*) malloc(space * sizeof (char *));
    char *heightStr = (char*) malloc((length - space) * sizeof (char *));

    memcpy(widthStr, line, space * sizeof(char));
    memcpy(heightStr, line+ (space * sizeof(char)), (length - space - 1) * sizeof(char));

    width = atoi(widthStr);
    height = atoi(heightStr);
    free(heightStr);
    free(widthStr);
}

void initMaze() {
    maze = (int**) malloc(height * sizeof(int*));
    for(int i = 0; i < 10; i++) {
        maze[i] = (int*) malloc(width * sizeof(int*));
    }
}


void loadMaze(char *path) {
    char *line;
    int length = 0;
    int row = 0;

    FILE *mazeFile = fopen(path, "r");
//    FILE *mazeFile = fopen("../maze.txt", "r");

    size_t buffer = 20;
    line = (char*) malloc(buffer * sizeof(char));

    while(length != -1) {
        length = (int)getline(&line, &buffer, mazeFile);
        if (row == 0) {
            getMazeWidthAndHeight(line, length);
            initMaze();
        } else {
            for (int i = 0; i < length; i++) {
                if ((int)line[i] == 32) {
                    maze[row-1][i] = 0;
                } else {
                    maze[row-1][i] = 1;
                }
            }
        }
        row++;
    };
}

void findStartAndEndPoint(){
    for(int i =0; i < 10; i ++) {
        if (maze[i][0] == 0) {
            start.x = 0;
            start.y = i;
        }
        if (maze[i][9] == 0) {
            end.x = 9;
            end.y = i;
        }
    }
}

void breathFirstSearch(int x, int y, struct Position pathRecord[], int step) {
    struct Position currentPosition;
    currentPosition.x = x;
    currentPosition.y = y;
    pathRecord[step] = currentPosition;
    if(x == end.x && y == end.y) {
        for (int i =0; i <= step; i++) {
            printf("Step%d: %d %d\n",i, pathRecord[i].y, pathRecord[i].x);
        }
        printf("The route exist");
        exit(0);
    }
    // go top
    if (y - 1 > 0 && maze[y - 1][x] == 0) {
        maze[y - 1][x] = 2;
        breathFirstSearch(x, y - 1, pathRecord, step +1);
    }
    // go bottom
    if (y + 1 < 10 && maze[y + 1][x] == 0) {
        maze[y + 1][x] = 2;
        breathFirstSearch(x, y + 1, pathRecord, step +1);
    }
    // go left
    if (x - 1 > 0 && maze[y][x - 1] == 0) {
        maze[y][x - 1] = 2;
        breathFirstSearch(x - 1, y, pathRecord, step +1);
    }
    // go right
    if (x + 1 < 10 && maze[y][x + 1] == 0) {
        maze[y][x + 1] = 2;
        breathFirstSearch(x + 1, y, pathRecord, step +1);
    }
};


int main(int argc, char*argv[]) {
    int step = 0;
    struct Position pathRecord[width * height];
    loadMaze(argv[1]);
    findStartAndEndPoint();
    printf("Maze Width: %d, Maze Height: %d\n", width, height);
    printf("Entrance y:%d x:%d\n",start.y, start.x);
    printf("Destination y:%d x:%d\n",end.y, end.x);
    breathFirstSearch(start.x, start.y, pathRecord, step);
    printf("No Path exist");
    exit(1);
}

